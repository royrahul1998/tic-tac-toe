#include<bits/stdc++.h>
using namespace std;

#define computer 1
#define human 2

#define computermove 'O'
#define humanmove 'X'

class tic_tac_toe{
    vector<vector<char>> board;
    public:

    tic_tac_toe(){
        for(int i=0;i<3;i++){
            vector<char> temp;
            for(int j=0;j<3;j++) temp.push_back(' ');
            board.push_back(temp);
        }
    }

    void showBoard(){
        cout<<"\n--------------------------------------------"<<endl;
        cout<<"\t\tTic Tac Toe"<<endl;
        cout<<"\n--------------------------------------------"<<endl;

        cout<<"\t\t-------------"<<endl;
        cout<<"\t\t| "<<board[0][0]<<" | "<<board[0][1]<<" | "<<board[0][2]<<" |"<<endl;
        cout<<"\t\t-------------"<<endl;
        cout<<"\t\t| "<<board[1][0]<<" | "<<board[1][1]<<" | "<<board[1][2]<<" |"<<endl;
        cout<<"\t\t-------------"<<endl;
        cout<<"\t\t| "<<board[2][0]<<" | "<<board[2][1]<<" | "<<board[2][2]<<" |"<<endl;
        cout<<"\t\t-------------"<<endl;
    }
    void showInstruction(){
        cout<<"\n Choose from 1-9 according to the specific cell Number and play: "<<endl;
        cout<<"\t\t-------------"<<endl;
        cout<<"\t\t| 1 | 2 | 3 |"<<endl;
        cout<<"\t\t-------------"<<endl;
        cout<<"\t\t| 4 | 5 | 6 |"<<endl;
        cout<<"\t\t-------------"<<endl;
        cout<<"\t\t| 7 | 8 | 9 |"<<endl;
        cout<<"\t\t-------------"<<endl;

    }

    void declareWin(int turn){
        if(turn == computer) cout<<"Computer Has Won"<<endl;
        else cout<<"Human Has Won"<<endl;
    }

    bool rowfilled(){
        for(int i=0;i<3;i++){
            if(board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0]!=' ') return true;
        }
        return false;
    }

    bool colfilled(){
        for(int i=0;i<3;i++){
            if(board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i]!=' ') return true;
        }
        return false;
    }

    bool diagonalfilled(){
        if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0]!= ' ') return true;
        if(board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2]!= ' ') return true;

        return false;
    }

    bool gameOver(){
        return rowfilled() || colfilled() || diagonalfilled();
    }
    //Minimax algorithm

    int minmax(int index, bool isComputer){
        int score=0;
        int bestscore=0;

        if(gameOver() == true){
            //if human win in minimax then -10 is send and when computer wins +10 is send and for draw 0 will be sent
            //isComputer true means previos state was for human , and human complete the game , so -10.
            if(isComputer) return -10;
            else return 10;
        }

        // it means draw
        if(index>=9) return 0;

        if(isComputer){
            //As we want to maximize computer score
            int bestScore=-999;
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    if(board[i][j] == ' '){
                        board[i][j] = computermove;
                        int score=minmax(index+1,false);
                        board[i][j] = ' ';
                        if(score>bestscore){
                            bestscore=score;
                        }
                    }
                }
            }
        }
        else{
            //As we want to minimize human score
            bestscore=999;
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    if(board[i][j] == ' '){
                        board[i][j] = humanmove;
                        int score=minmax(index+1,true);
                        board[i][j] = ' ';
                        if(score<bestscore){
                            bestscore=score;
                        }
                    }
                }
            }
        }
        return bestscore;
    }
    //Backtracking algorithm to find best move for computer
    int bestMove(int index){
        int x=-1;
        int y=-1;
        int bestscore=-999;

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(board[i][j] == ' '){
                    board[i][j] = computermove;
                    int score=minmax(index+1,false);
                    board[i][j] = ' ';
                    if(score>bestscore){
                        bestscore=score;
                        x=i;
                        y=j;
                    }
                }
            }
        }
        return x*3+y;
    }

    void startGame(int turn){
        int index=0;
        int r=0;
        int c=0;

        showBoard();
        while(gameOver() == false && index<9){
            
            int move;
            if(turn == computer){
                move=bestMove(index);
                r=move/3;
                c=move%3;
                board[r][c]=computermove;
                
                cout << "\033[2J\033[1;1H"; // Clear Screen
                showBoard();
                cout<<"\nComputer has put "<<computermove<<" at place "<<move+1<<endl;
                index++;
                turn=human;
            }

            else{
                showInstruction();
                cout<<"You can Enter the following positions: ";
                for(int i=0;i<3;i++){
                    for(int j = 0;j<3;j++){
                        if(board[i][j]==' ')    cout<<i*3+j+1<<" ";
                    }
                }
                cout<<endl;
                cout<<"Enter your position: ";
                cin>>move;

                move--;
                r=move/3;
                c=move%3;
                if(board[r][c]==' '){
                    if(move>=0 && move<9){
                        board[r][c]=humanmove;
                        cout << "\033[2J\033[1;1H"; // Clear Screen
                        showBoard();
                        index++;
                        turn=computer;
                    }
                    else{
                        cout<<"Invalid Position \n";
                    }
                }
                else{
                    cout<<"The Position is already filled. Please try another position \n";
                }
            }
        }
        if(gameOver()==false && index == 9){
            cout<<"Nice Play. It's a Draw\n";
        }
        else{
            turn == human ? declareWin(computer) : declareWin(human);
        }
    }

};

int main(){
    
    while(1){
        
        char choice;
        cout<<"You want your first move? (y/n): ";
        cin>>choice;
        tic_tac_toe obj;
        if(choice=='n') obj.startGame(computer);
        else if(choice=='y') obj.startGame(human);
        else cout<<"Invalid Input"<<endl;

        char c;
        cout<<"You want to quit or not? (y/n) : ";
        
        cin>>c;
        if(c!='n') break;
        cout << "\033[2J\033[1;1H"; // Clear Screen
    }
}
